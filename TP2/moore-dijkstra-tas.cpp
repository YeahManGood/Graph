#include <iostream>
#include <stdlib.h>
#include <stack>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;


vector<vector<int>> c;
vector<int> pere;
vector<int> d;

void lireFichier (string fileName){
	ifstream file;
	string currentLine;
	istringstream input;
	
	vector < int > values;
	int value;

	file.open(fileName);
	
	while (getline(file, currentLine)){
		input.str(currentLine);
		while(input >> value){
			values.push_back(value);
		}
		c.push_back(values);
		input.clear();
		values.erase(values.begin(), values.end());
	}
}

void display_shortest_paths(vector <int> pere){
	for (int i = 0; i < pere.size(); i++){
		cout << i+1 << " ";
	}
	cout << endl;
	for (int i = 0; i < pere.size(); i++){
		cout << pere[i] + 1 << " " ;
	}
	cout << endl;
}

void display_all (vector <int> Cb){
	cout << "#################################" << endl;
	cout << "\t ->: Liste des sommets à parcourir" << endl;
	for (int i = 0; i < Cb.size(); i++){
		cout << Cb[i] << " ";
	}

	cout << endl;


	cout << endl << "Pere: \t ";
	
	display_shortest_paths(pere);

	cout << endl << "Distances : \t ";

	for (int i = 0; i < d.size(); i++){
		cout << d[i] << " ";
	}
	cout << endl;
}

bool cmp (const int & i, const int & j){

    return (d[i] < d[j]);

}

void mooreDijsktra (int s = 0){
	// sort_heap


	vector <int> C(s);
	vector <int> Cb;
	// Initialisation des plus courts chemins (INT_MAX = infini)
	// Et initialisation des chemins
	for (int i = 0; i < c.size(); i++){
		if (i != s){
			d.push_back(INT_MAX);
		}else{
			d.push_back(0);
		}
		Cb.push_back(i);
		pere.push_back(0);
	}

	make_heap(Cb.begin(), Cb.end(),cmp);
	
	int j;
	cout << " -----------------------" << endl;
	for (int l = 0; l < c.size(); l++){
		
		j = Cb.back();
		Cb.pop_back();
		pop_heap(Cb.begin(), Cb.end(), cmp);

		display_all(Cb);
		cout << "j: " << j << endl;
		for (int i : Cb){
			if (c[i][j] != 0){
				if (d[j] + c[i][j] < d[i]){
					d[i] = d[j] + c[i][j];
					pere[i] = j;
				}
			}
		}

		make_heap(Cb.begin(), Cb.end(), cmp);

		//display_all(Cb);
	}
}






int main(int argc, char const *argv[]){
	// Remplissage de la matrice des distances
	if (argc == 2){
		lireFichier (argv[1]);
	}else{
		cout << "Il faut donner un seul argument en entrée" << endl;
		return 0;
	}
	cout << c.size() << endl;
	//vector <int> Cb;
	//= {7,10,3,9};
	//vector <int> d; 
	//= {0,2,7,1,3,6,10,5,12,-1,14};
	//int res = min_element(d, Cb);
	//cout << res << endl;
	mooreDijsktra(); 
	return 0;
}
