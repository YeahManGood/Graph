#include <iostream>
#include <stdio.h>

using namespace std;

class Arc {
	private:
		int sommet;
		int dist;
	public:
		Arc(int _sommet, int _dist){
			this->sommet = _sommet;
			this->dist = _dist;
		}
		Arc (){}

		int getSommet(){
			return this->sommet;
		}
		int getDist(){
			return this->dist;
		}
};
