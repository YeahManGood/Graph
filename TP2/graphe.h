#include <iostream>
#include <stdio.h>
#include <vector>
#include "arc.h"
#include <string>
#include <fstream>
#include <climits>
#include <sstream>


using namespace std;

class Graphe {
	private:
		vector<vector<Arc>> arcs;
	public:
		Graphe(){}

		Graphe (string fileName, int nbNoeuds){
			arcs = vector<vector<Arc>>(nbNoeuds, vector<Arc>());
			cout << "Graphe initialisé: " << arcs.size() << endl;
			lireFichier(fileName);
		}

		void lireFichier (string fileName){
			ifstream file;
			string currentLine;
			istringstream input;
			vector < int > values;
			int value;
			int depart;
			file.open(fileName);
			while (getline(file, currentLine)){
				input.str(currentLine);
				while(input >> value){
					values.push_back(value);
				}
				depart = values[0];
				this->arcs[depart].push_back(Arc(values[1], values[2]));
				input.clear();
				values.erase(values.begin(), values.end());
			}
		}

		void display(){
			cout << "Affichage du graphe, taille: " << this->arcs.size() << endl;
			for (int i =1 ; i < this->arcs.size(); i++){
				cout << "indice:" << i;
				for (int j = 0; j<this->arcs[i].size(); j++){
					cout << "\tsommet:" << this->arcs[i][j].getSommet() << 
					"dist:" << this->arcs[i][j].getDist();
				}
				cout << endl;
			}
		}
		
		arc getArc(int i, int j){
			for (int k = 0; k < arcs[i].size())
			return arcs[i];
		}
};
