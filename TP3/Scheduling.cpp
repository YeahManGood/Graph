#include <iostream>
#include <stdlib.h>
#include <stack>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <climits>
#include <algorithm>

#define A 6
#define B 7

using namespace std;

struct Edge 
{ 
	int src, dest, weight; 
}; 

struct Graph 
{ 
	int V, E; 
	struct Edge* edge; 
};

struct Graph* createGraph(int V, int E) 
{ 
	struct Graph* graph = new Graph; 
	graph->V = A; 
	graph->E = B; 
	graph->edge = new Edge[E]; 
	return graph; 
}

void createEdge(Graph* graph, int head, int s, int d, int w){
	graph->edge[head].src = s; 
	graph->edge[head].dest = d; 
	graph->edge[head].weight = w; 
}

struct Graph* graph = createGraph(A, B);

void displayEdge(){
	
	printf("####### Graph building #######\n\n");
	printf("Source        Destination   Duration\n"); 
	for ( int i = 0 ; i < 6 ; i++){
		printf("%d             %d             %d\n", graph->edge[i].src, graph->edge[i].dest, graph->edge[i].weight);
		
	}
}

void graphepotentieltache(string fileName){
	ifstream file;
	string currentLine;
	istringstream input;

	vector < int > values;
	int value;
	int count = 0;
	file.open(fileName.c_str());
	
	while (getline(file, currentLine)){
		input.clear();
		values.erase(values.begin(), values.end());
		input.str(currentLine);	
		for (int i = 0; i < 3; i++) {
			input >> value;
			values.push_back(value);
		}
		createEdge(graph, count, values[1], values[0], values[2]);
		count ++;
		values.erase(values.begin(), values.end());
	}
}



void displayEarlierStartingDate(int d[]){
	printf("####### Earlier Starting Date #######\n\n");
	printf("Tasks         Earlier Starting Date\n"); 
	for (int i = 0; i < A; i++) 
		printf("%d             %d\n", i, d[i]); 
	printf("\n");
}

void displayLatestStartingDate(int d[]){
	
	printf("####### Latest Starting Date #######\n\n");
	printf("Tasks         Latest Starting Date\n"); 
	for (int i = 0; i < A; i++) 
		printf("%d             %d\n", i, d[i]);
	printf("\n");
}


 
int maxDistance(int dist[]) { 
	
	int max = INT_MIN, max_index; 
	for (int i = 0; i < A; i++) 
		if (dist[i] >= max) 
			max = dist[i], max_index = i; 
	return max_index; 
}

void criticalPath(int esd[], int lsd[]){
	
	printf("####### Critical Path #######\n\n");
	printf("Critical Path : ");
	for (int i = 0; i < A; i++) {
		if(esd[i]-lsd[i]==0)
			cout << i << "->";
    }
    cout <<endl;
    printf("\n");
}

void latestStartingDate(int esd[]) {
	
	int lsd[A];
	for (int cpt = A-1; cpt >= 0; cpt--) {	 
        int u = graph->edge[cpt+1].src; 
		int v = graph->edge[cpt+1].dest; 
		int w = graph->edge[cpt+1].weight; 	
		lsd[u] = esd[v]-w;
	}
	lsd[0] = esd[0];
	lsd[A-1]=esd[A-1];
	displayLatestStartingDate(lsd);	
	criticalPath(esd, lsd);
	
}

void earlierStartingDate() { 
	
	int esd[A];
    int pere[A];
	
	for (int i = 0; i < A; i++) {
		esd[i], pere[i] = INT_MIN, -1;
    }
    pere[0] = -1;
	esd[0] = 0; 

	for (int cpt = 0; cpt < A; cpt++) { 
        int i = maxDistance(esd); 
        int u = graph->edge[cpt].src; 
		int v = graph->edge[cpt].dest; 
		int weight = graph->edge[cpt].weight; 
		for (int j = 0; j < A; j++) 
			if (esd[i]+weight > esd[j]) {
				esd[v] = esd[u] + weight; 
				pere[v] = u;
			}
	}		
	displayEarlierStartingDate(esd);
	latestStartingDate(esd);
} 



int main(int argc, char const *argv[]) { 
	
	if (argc == 2){
		graphepotentieltache(argv[1]);
	}else{
		cout << "Il faut donner un seul argument en entrée" << endl;
		return 0;
	}
	displayEdge();
	earlierStartingDate(); 
	return 0; 
}
