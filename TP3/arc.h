#include <iostream>
#include <stdio.h>

using namespace std;

class Arc {
	private:
		vector <int> sommet;
		int dist;
	public:
		Arc (int _sommet, int _dist){
			this->sommet.push_back(_sommet);
			this->dist = _dist;
		}
		Arc (){}

		int getSommet(int indice){
			return this->sommet[indice];
		}
		int getDist(){
			return this->dist;
		}

		void setSommet(int _sommet){
			this->sommet.push_back(_sommet);
		}

		int getTaille(){
			return this->sommet.size();
		}

		void setDist (int _dist ){
			this->dist = _dist;
		}
};
