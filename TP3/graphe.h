#include <iostream>
#include <stdio.h>
#include <vector>
#include "arc.h"
#include <string>
#include <fstream>
#include <climits>
#include <sstream>


using namespace std;

class Graphe {
	private:
		vector<Arc> arcs;
	public:
		Graphe(){}

		Graphe (string precedents, string duree){
			graphepotentieltache(precedents, duree);
			cout << "Graphe initialisé: " << arcs.size() << endl;
		}

		void graphepotentieltache(string precedents, string duree){
			ifstream preced;
			ifstream dur;
			string currentLine;
			string currentDist;
			istringstream input;

			vector < int > values;
			int value, dist;
			int count = 0;
			preced.open(precedents.c_str());
			dur.open(duree.c_str());

			// Comme on a es vecteurs, on peut skip la première ligne
			// On l'affiche car c'est quand même mieux
			getline(preced, currentLine);
			cout << "Le graphe comporte " << currentLine << " noeuds" << endl;

			while (getline(preced, currentLine)){
				// On clear tout
				input.clear();
				values.erase(values.begin(), values.end());
				input.str(currentLine);
				while (input >> value) {
					values.push_back(value);
				}

				getline(dur, currentDist);
				input >> dist;
				Arc * a = new Arc(values[0], dist);

				//On parcours tout le string sauf le 1er
				for (int som = 1; som < values.size(); som++){
					arcs[values[0]].setSommet(values[som]);
				}

				count ++;
				values.erase(values.begin(), values.end());
			}
		}

		void display(){
			cout << "Affichage du graphe, taille: " << this->arcs.size() << endl;
			for (int i =1 ; i < this->arcs.size(); i++){
				for (int j = 0; j < arcs[i].getTaille(); j++){
					cout << i << "--- " << arcs[i].getDist() <<" ---" << arcs[i].getSommet(j);
				}
				cout << endl;
			}
		}
	/*	
		Arc getArc(int i, int j){
			for (int k = 0; k < arcs[i].size())
			return arcs[i];
		}*/
};
